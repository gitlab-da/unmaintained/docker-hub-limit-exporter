FROM python:3-alpine

COPY . /src
RUN pip install -r /src/requirements.txt

WORKDIR /src
ENV PYTHONPATH '/src/'

CMD ["python", "/src/collector.py"]
